
plugin.tx_hivecptcntshare_hivecptcntsharerender {
    view {
        # cat=plugin.tx_hivecptcntshare_hivecptcntsharerender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_share/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntshare_hivecptcntsharerender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_share/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntshare_hivecptcntsharerender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_share/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntshare_hivecptcntsharerender//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntshare_hivecptcntsharerender {
    settings {
        share {
            # strpos(...) for each comma separated value
            excludeIconsForDomainParts =
            facebook {
                active = 1
                icon = /typo3conf/ext/hive_cpt_cnt_share/Resources/Public/Icons/Provider/Png/48px/Rectangular/facebook-dreamstale25.png
            }
            twitter {
                active = 1
                icon = /typo3conf/ext/hive_cpt_cnt_share/Resources/Public/Icons/Provider/Png/48px/Rectangular/twitter2-dreamstale72.png
            }
            linkedin {
                active = 1
                icon = /typo3conf/ext/hive_cpt_cnt_share/Resources/Public/Icons/Provider/Png/48px/Rectangular/linkedin-dreamstale45.png
            }
            xing {
                active = 1
                icon = /typo3conf/ext/hive_cpt_cnt_share/Resources/Public/Icons/Provider/Png/48px/Rectangular/xing-dreamstale82.png
            }
            mailto {
                active = 1
                icon = /typo3conf/ext/hive_cpt_cnt_share/Resources/Public/Icons/Provider/Png/48px/Rectangular/mail-dreamstale47.png
            }
        }
    }
}
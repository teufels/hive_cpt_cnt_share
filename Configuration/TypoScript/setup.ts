
plugin.tx_hivecptcntshare_hivecptcntsharerender {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_share/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_share/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_share/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptcntshare._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-cnt-share table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-cnt-share table th {
        font-weight:bold;
    }

    .tx-hive-cpt-cnt-share table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntshare_hivecptcntsharerender {
    settings {
        share {
            excludeIconsForDomainParts = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.excludeIconsForDomainParts}
            facebook {
                active = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.facebook.active}
                icon = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.facebook.icon}
            }
            twitter {
                active = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.twitter.active}
                icon = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.twitter.icon}
            }
            linkedin {
                active = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.linkedin.active}
                icon = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.linkedin.icon}
            }
            xing {
                active = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.xing.active}
                icon = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.xing.icon}
            }
            mailto {
                active = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.mailto.active}
                icon = {$plugin.tx_hivecptcntshare_hivecptcntsharerender.settings.share.mailto.icon}
            }
        }
    }
}

##
## Lib for Share buttons
##
lib.tx_hivecptcntshare_hivecptcntsharerender = COA
lib.tx_hivecptcntshare_hivecptcntsharerender {
    10 = USER
    10 {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        extensionName = HiveCptCntShare
        pluginName = Hivecptcntsharerender
        vendorName = HIVE
        controller = Render
        action = render
        settings =< plugin.tx_hivecptcntshare_hivecptcntsharerender.settings
        persistence =< plugin.tx_hivecptcntshare_hivecptcntsharerender.persistence
        view =< plugin.tx_hivecptcntshare_hivecptcntsharerender.view
    }
}